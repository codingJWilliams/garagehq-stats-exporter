# GarageHQ Stats Exporter

This is a prometheus exporter for the GarageHQ S3-compatible server software. The built in metrics endpoint does not export bucket size, so I've made this custom exporter so that I can track this through Prometheus.

An example of the exported stats is:

 - garage_host_online{hostname="garage-0"} 1.0
 - garage_bucket_bytes{bucket="cool-bucket"} 438565.0
 - garage_bucket_objects{bucket="cool-bucket"} 34556