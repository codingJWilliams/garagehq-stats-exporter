from prometheus_client.core import GaugeMetricFamily, CounterMetricFamily, REGISTRY
from prometheus_client.registry import Collector
from prometheus_client import start_http_server
import requests
import os

adminEndpoint = os.environ.get("GARAGE_ADMIN_ENDPOINT")
adminToken = os.environ.get("GARAGE_ADMIN_TOKEN")

headers = {
    "Authorization": f"Bearer {adminToken}"
}

class GarageCollector(Collector):
    def collect(self):
        status = requests.get(f"{adminEndpoint}/v1/status", headers=headers).json()

        hostsOnline = GaugeMetricFamily('garage_host_online', 'Tracks online status of GarageHQ hosts', labels=["hostname"])
        for h in status['knownNodes']:
            hostsOnline.add_metric([h['hostname']], 1 if h['isUp'] else 0)
        yield hostsOnline

        bucketBytes = GaugeMetricFamily('garage_bucket_bytes', 'Size of GarageHQ buckets in bytes', labels=["bucket"])
        bucketObjects = GaugeMetricFamily('garage_bucket_objects', 'Number of objects in each GarageHQ bucket', labels=["bucket"])
        buckets = requests.get(f"{adminEndpoint}/v1/bucket?list", headers=headers).json()
        for b in buckets:
            bucket = requests.get(f"{adminEndpoint}/v1/bucket?id={b['id']}", headers=headers).json()
            bucketBytes.add_metric([bucket['globalAliases'][0]], bucket['bytes'])
            bucketObjects.add_metric([bucket['globalAliases'][0]], bucket['objects'])
        yield bucketBytes
        yield bucketObjects

REGISTRY.register(GarageCollector())
server, t = start_http_server(8000)
print("Listening for metrics on :8000")
t.join()